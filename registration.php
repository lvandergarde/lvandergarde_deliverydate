<?php
/**
 * Registers the Lvandergarde_DeliveryDate module
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Lvandergarde_DeliveryDate',
    __DIR__
);
