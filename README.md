# README #

Installation instructions (when not using Magento Connect):

1. Create the following folder structure:
app/code/Lvandergarde/DeliveryDate

2. Checkout this repository in the mentioned folder structure

3. Execute the following commands in the root of your Magento installation 

- php -f bin/magento module:enable Lvandergarde_DeliveryDate --clear-static-content
- rm -rf var/generation/* var/cache/*
- php -f bin/magento setup:upgrade
- php -f bin/magento setup:di:compile
- php -f bin/magento indexer:reindex