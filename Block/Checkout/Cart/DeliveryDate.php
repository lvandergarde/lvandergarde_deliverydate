<?php
/**
 * DeliveryDate module
 * @author Lennart van der Garde
 */
namespace Lvandergarde\DeliveryDate\Block\Checkout\Cart;

use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Checkout\Block\Cart\Additional\Info;
use Magento\Framework\View\Element\Template;

/**
 * Class DeliveryDate
 *
 * @package Lvandergarde\DeliveryDate\Block\Checkout\Cart
 */
class DeliveryDate extends Info
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    protected $stockItemRepo;

    /**
     * DeliveryDate constructor
     *
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        StockItemRepository $stockItemRepository,
        Template\Context $context,
        array $data = [])
    {
        $this->timezone = $context->getLocaleDate();
        $this->stockItemRepo = $stockItemRepository;
        parent::__construct($context, $data);
    }

    /**
     * Gets the delivery date attribute from the product in the quote
     *
     * @return string
     */
    protected function getFormattedDeliveryDate()
    {
        $formattedDeliveryDate = '';
        $item = $this->getItem();

        $deliveryDate = $item->getProduct()->getOutOfStockDeliveryDate();
        if ($deliveryDate) {
            $formattedDeliveryDate = $this->timezone->formatDate($deliveryDate, 3);
        }

        return $formattedDeliveryDate;
    }

    /**
     * Creates the notice that is visible on the cart page
     * Returns an empty string if no delivery date is set or selected qty is lower than stock
     *
     * @return string
     */
    public function getDeliveryNotice()
    {
        $html = '';
        if (!$this->checkStockAvailability() && $this->getFormattedDeliveryDate()) {
            $html = __(
                'This product is not in stock for the selected amount. The expected delivery date is: %1',
                $this->getFormattedDeliveryDate()
            );
        }

        return $html;
    }

    /**
     * Checks if the stock is higher than the selected quantity
     *
     * @return bool true if product stock is higher than quantity
     */
    protected function checkStockAvailability()
    {
        $item = $this->getItem();
        $itemId = $item->getId();
        $productStock = $this->getStockItem($itemId)->getQty();
        $quoteItemQty = $item->getProduct()->getQuoteItemQty();

        return $productStock >= $quoteItemQty;
    }

    /**
     * Get stock object for specified productId parameter
     *
     * @param $productId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     */
    protected function getStockItem($productId)
    {
        return $this->stockItemRepo->get($productId);
    }
}

